//
// Created by Jerzy on 2019-05-13.
//

#ifndef WEATHER_STATION_HTTP_H
#define WEATHER_STATION_HTTP_H

#endif //WEATHER_STATION_HTTP_H

//char request[200];

enum HTTP_REQUEST_TYPE {
    POST = 1,
    GET = 2,
    PUT = 3,
    DELETE = 4,
    UPDATE = 5
};



typedef struct HTTP_REQUEST HTTP_REQUEST;

struct HTTP_REQUEST {
    enum HTTP_REQUEST_TYPE rType;
    char * host;
    char * path;
    char * headers;
    char * data;

    char * (*parseRequest)(HTTP_REQUEST * req);
};

char * parseRequest(HTTP_REQUEST * req);
HTTP_REQUEST newHttpRequest();