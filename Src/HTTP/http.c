//
// Created by Jerzy on 2019-05-13.
//

#include <stdio.h>
#include <memory.h>
#include "http.h"


HTTP_REQUEST newHttpRequest() {
    HTTP_REQUEST req;
    req.parseRequest = parseRequest;
    return req;
}

#define HTTP_REQ_INSERT "POST /measurement/insert HTTP/1.1\r\nHost: 192.168.0.115\r\nAccept: */*\r\nConnection: keep-alive\r\nContent-Type: application/json\r\nContent-Length: %d\r\n\r\n"
char request[400];

char * parseRequest(HTTP_REQUEST * req) {
    switch (req->rType) {
        case POST:
            sprintf(request, "POST");
            break;
        case GET:
            sprintf(request, "GET");
            break;
        default:
            break;
    }

    // PATH
    sprintf(request, "%s %s HTTP/1.1\r\n", request, req->path);

    // HOST
    sprintf(request, "%sHost: %s\r\n", request, req->host);

    // HEADERS
    sprintf(request, "%s%s\r\n", request, req->headers);

    if ( req->rType == POST) {
        sprintf(request, "%sContent-Length: %d\r\n\r\n", request, strlen(req->data));
        sprintf(request, "%s%s", request, req->data);
    }

    return request;
}