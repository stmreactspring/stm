#include "dht11.h"

int DHT11_init(struct DHT11_Data* dev) {
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	//Initialise GPIO DHT11
	GPIO_InitStruct.Pin = dev->pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(dev->port, &GPIO_InitStruct);

	return 0;
}

int DHT11_read(struct DHT11_Data* dev) {

	TIM_TypeDef * timer = (*dev->timer).Instance;

	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = dev->pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(dev->port, &GPIO_InitStruct);

	//Initialisation
	uint8_t i, j, temp;
	uint8_t data[5] = {0x00, 0x00, 0x00, 0x00, 0x00};

	//Put LOW for at least 18ms
	HAL_GPIO_WritePin(dev->port, dev->pin, GPIO_PIN_RESET);
	//wait 18ms

	HAL_Delay(18);

	//Put HIGH for 20-40us
	HAL_GPIO_WritePin(dev->port, dev->pin, GPIO_PIN_SET);

	//wait 40us
	timer->CNT = 0;
	while(timer->CNT <= 40);
	//End start condition

	GPIO_InitStruct.Pin = GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	//DHT11 ACK
	//should be LOW for at least 80us
	//while(!HAL_GPIO_ReadPin(dev->port, dev->pin));
	timer->CNT = 0;
	while(!HAL_GPIO_ReadPin(dev->port, dev->pin)) {
		if(timer->CNT > 100) {
			sprintf(dev->error, "ERR1 ");
			return DHT11_ERROR_TIMEOUT;
		}
	}

	//should be HIGH for at least 80us
	//while(HAL_GPIO_ReadPin(dev->port, dev->pin));
	timer->CNT = 0;
	while(HAL_GPIO_ReadPin(dev->port, dev->pin)) {
		if(timer->CNT > 100) {
			sprintf(dev->error, "ERR 2", timer->CNT);
			return DHT11_ERROR_TIMEOUT;
		}
	}

	//Read 40 bits (8*5)
	for(j = 0; j < 5; ++j) {
		for(i = 0; i < 8; ++i) {

			//LOW for 50us
			while(!HAL_GPIO_ReadPin(dev->port, dev->pin));
			timer->CNT = 0;
			while(!HAL_GPIO_ReadPin(dev->port, dev->pin)) {
				if(timer->CNT > 60) {
					sprintf(dev->error, "ERR 3", timer->CNT);
					return DHT11_ERROR_TIMEOUT;
				}
			}

			//Start counter
			timer->CNT = 0;

			//HIGH for 26-28us = 0 / 70us = 1
			while(HAL_GPIO_ReadPin(dev->port, dev->pin)) {
				if(timer->CNT > 80) {
					sprintf(dev->error, "ERR 4", timer->CNT);
					return DHT11_ERROR_TIMEOUT;
				}
			}

			//Calc amount of time passed
			temp = timer->CNT;

			//shift 0
			data[j] = data[j] << 1;

			//if > 30us it's 1
			if(temp > 40)
				data[j] = data[j]+1;
		}
	}

	//verify the Checksum
	if(data[4] != (data[0] + data[2]))
		return DHT11_ERROR_CHECKSUM;

	//set data
	dev->temparature = data[2];
	dev->humidity = data[0];

	return DHT11_SUCCESS;
}


int DHT22_read(struct DHT22_Data* dev) {

	TIM_TypeDef * timer = (*dev->timer).Instance;

	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = dev->pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(dev->port, &GPIO_InitStruct);

	//Initialisation
	uint8_t i, j, temp;
	uint8_t data[5] = {0x00, 0x00, 0x00, 0x00, 0x00};

	//Put LOW for at least 18ms
	HAL_GPIO_WritePin(dev->port, dev->pin, GPIO_PIN_RESET);
	//wait 18ms

	HAL_Delay(5);

	//Put HIGH for 20-40us
	HAL_GPIO_WritePin(dev->port, dev->pin, GPIO_PIN_SET);
	timer->CNT = 0;
	while(timer->CNT <= 40);

	//End start condition

	GPIO_InitStruct.Pin = GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	//DHT11 ACK
	//should be LOW for at least 80us
	//while(!HAL_GPIO_ReadPin(dev->port, dev->pin));
	timer->CNT = 0;
	while(!HAL_GPIO_ReadPin(dev->port, dev->pin)) {
		if(timer->CNT > 60) {
			sprintf(dev->error, "ERR1 ");
			return DHT11_ERROR_TIMEOUT;
		}
	}

	//should be HIGH for at least 80us
	//while(HAL_GPIO_ReadPin(dev->port, dev->pin));
	timer->CNT = 0;
	while(HAL_GPIO_ReadPin(dev->port, dev->pin)) {
		if(timer->CNT > 100) {
			sprintf(dev->error, "ERR 2", timer->CNT);
			return DHT11_ERROR_TIMEOUT;
		}
	}

	//Read 40 bits (8*5)
	for(j = 0; j < 5; ++j) {
		for(i = 0; i < 8; ++i) {

			//LOW for 50us
			while(!HAL_GPIO_ReadPin(dev->port, dev->pin));
			timer->CNT = 0;
			while(!HAL_GPIO_ReadPin(dev->port, dev->pin)) {
				if(timer->CNT > 60) {
					sprintf(dev->error, "ERR 3", timer->CNT);
					return DHT11_ERROR_TIMEOUT;
				}
			}

			//Start counter
			timer->CNT = 0;

			//HIGH for 26-28us = 0 / 70us = 1
			while(HAL_GPIO_ReadPin(dev->port, dev->pin)) {
				if(timer->CNT > 80) {
					sprintf(dev->error, "ERR 4", timer->CNT);
					return DHT11_ERROR_TIMEOUT;
				}
			}

			//Calc amount of time passed
			temp = timer->CNT;

			//shift 0
			data[j] = data[j] << 1;

			//if > 30us it's 1
			if(temp > 40)
				data[j] = data[j]+1;
		}
	}

	for ( int i = 0; i < 5; i++)
		dev->data[i] = data[i];

	dev->temparature = (data[2] << 8) + data[3];
	dev->humidity = (data[0] << 8) + data[1];

	int checksum = (data[0] + data[1] + data[2] + data[3]) & 0xFF;

	//verify the Checksum
	if(data[4] != checksum) {
		return DHT11_ERROR_CHECKSUM;
	}

	//set data


	return DHT11_SUCCESS;
}
