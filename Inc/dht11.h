#ifndef __DHT11_H
#define __DHT11_H
#include "stm32l1xx.h"
#include <stm32l1xx_hal_tim.h>


#define DHT11_SUCCESS         1
#define DHT11_ERROR_CHECKSUM  2
#define DHT11_ERROR_TIMEOUT   3

#endif

typedef struct DHT11_Data {
	uint8_t temparature;
	uint8_t humidity;
	GPIO_TypeDef* port;
	uint16_t pin;
    TIM_HandleTypeDef * timer;
    char error[100];
} DHT11_Data;

typedef struct DHT22_Data {
	uint16_t temparature;
	uint16_t humidity;
	GPIO_TypeDef* port;
	uint16_t pin;
	uint8_t data[5];
	TIM_HandleTypeDef * timer;
	char error[100];
} DHT22_Data;

int DHT11_init(struct DHT11_Data* dev);
int DHT11_read(struct DHT11_Data* dev);
int DHT22_read(struct DHT22_Data* dev);